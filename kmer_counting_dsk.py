#!/usr/bin/python3

import os
import sys
import inspect
import subprocess
#import time


currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.dirname(currentdir)+"/synthesis_modules")
import dna_file_reader as dfr


def count_kmers(input_path: str, kmer_size: int, min_occ: int) -> dict:
    """
    use the dsk tool to count the occurrences of each kmers in the reads
    returns the dict of kmer occurrences filtered with a threshold of minimum occurrence
    """
    #start = time.time()
    if not os.path.isfile(input_path) or os.stat(input_path).st_size == 0:
        print(input_path,"is an empty file")
        return {}

    dsk_script_path = currentdir+"/dsk/build/bin/dsk"
    dsk_tempfile_path = currentdir+"/dsk/tmp/kmer_count"

    kmer_count_command = dsk_script_path+' -file '+input_path+' -out '+dsk_tempfile_path+' -kmer-size '+ str(kmer_size)+' -abundance-min '+str(min_occ)+' -verbose 0'
    subprocess.call('/bin/bash -c "$DSK"', shell=True, env={'DSK': kmer_count_command})

    # convert the result into a txt file -> lines of "kmer occurrences"
    dsk_ascii_script_path = currentdir+"/dsk/build/bin/dsk2ascii" # -file dsk_tempfile_path -out test.txt
    dsk_tempfile_txt_path = currentdir+"/dsk/tmp/kmer_count.txt"

    count_convert_command = dsk_ascii_script_path+' -file '+dsk_tempfile_path+'.h5 -out '+dsk_tempfile_txt_path+' -verbose 0'
    subprocess.call('/bin/bash -c "$CONVERT"', shell=True, env={'CONVERT': count_convert_command})

    #count_time = time.time() - start
    #print("\ncounting",count_time,"seconds")
    #start = time.time()

    kmer_occurrences_dict = {}
    # parse the file of kmer occurrences and save the lines in a dictionary
    with open(dsk_tempfile_txt_path) as count_file:
        for line in count_file:
            kmer_sequence, occurrences_str = line.split(" ")
            occurrences = int(occurrences_str)

            kmer_occurrences_dict[kmer_sequence] = occurrences
            # also save the occurrences for the reverse complement of the kmer since dsk use canonical representation of kmers
            kmer_occurrences_dict[dfr.reverse_complement(kmer_sequence)] = occurrences


    #print("\nloading",time.time() - start,"seconds")

    return kmer_occurrences_dict

