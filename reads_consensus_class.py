#!/usr/bin/python3

import os
import sys
import inspect
import time
import argparse
import graphviz

import kmer_counting_dsk as dsk

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.dirname(currentdir)+"/synthesis_modules")
import dna_file_reader as dfr

"""
used to get the consensus sequence from a .fastq reads file of an ordered assembly and a couple of primers
can find multiple consensus if different files are merged with the same primers
"""


def build_all_consensus(kmer_occ_dict: dict, kmer_size: int, start_primer: str, stop_primer: str, seq_size: int) -> None:
    """
    init the class variables and constants
    and starts a consensus from the start primer
    """


    # -- constants for all consensus objects -- #
    Consensus.kmer_occurrences_dict = kmer_occ_dict # dict of occurrences of every kmers in the reads (filtered by a minimum number of occurrences)
    Consensus.kmer_size = kmer_size # size of kmers counted in the reads
    Consensus.start_sequence = start_primer # the start of any consensus path
    Consensus.stop_sequence = dfr.reverse_complement(stop_primer) # ending sequence of every consensus path
    Consensus.seq_size = seq_size # espected length (in bases) of the result consensus sequence

    # -- var to be completed over time -- #
    Consensus.kmers_dict = {} # dict of kmers present in the consensus; key=kmer, value=consensus_id # a kmer can only appear in 1 unique consensus,
                    # if it would be shared between 2 consensus, a link is created and one consensus will be part of the other's childs

    Consensus.consensus_dict = {} # dict key=consensus_id: value=Consensus object
    Consensus.consensus_number = 0 # number of consensus objects created, used to give a consensus id to new consensus
    Consensus.list_of_consensus_to_explore = [] # list of [score, father_name, split_index, first_kmer]
                                      # sorted by the score (=occ of the start kmer)
                                      # use to create a new consensus object from start kmer splitting from a father consensus or from the "start"
                                      # new consensus to explore are inserted to keep the ordering


    # find all starters (kmers that begins with the start sequence)
    potential_starters_dict = dict((k,v) for k,v in Consensus.kmer_occurrences_dict.items() if k.startswith(start_primer))

    # list of starters kmers sorted by number of occurrences in the reads (smaller first)
    Consensus.starters_list = sorted(potential_starters_dict, key=potential_starters_dict.get, reverse=False)

    #print(potential_starters_dict)
    # add the starters as consensus to explore
    for start_kmer in Consensus.starters_list:
        start_kmer_score_occ = Consensus.kmer_occurrences_dict[start_kmer]
        Consensus.list_of_consensus_to_explore.append([start_kmer_score_occ, "start", 0, start_kmer])

    # continue to explore new consensus
    while Consensus.list_of_consensus_to_explore:# and Consensus.consensus_number <= 30000:

        [score, father_id, split_index, start_kmer] = Consensus.list_of_consensus_to_explore.pop() # get last element # the one with higher score
        create_new_consensus(father_id, split_index, start_kmer) # run the new consensus


def create_new_consensus(father_consensus_id: str, split_kmer_index: int, child_starting_kmer: str) -> None:
    """
    create a new consensus starting from the beginning, or from another consensus
    """
    # if the starting kmer is already contained in a consensus, just create a link instead of a new child consensus
    if child_starting_kmer in Consensus.kmers_dict:
        child_consensus_id = Consensus.kmers_dict[child_starting_kmer]
        child_consensus = Consensus.consensus_dict[child_consensus_id]
        index_child_starting_kmer = child_consensus.kmer_to_position_dict[child_starting_kmer]
        #print("link child path",father_consensus_id,"->",child_consensus_id)
    # if the kmer has never been seen, create a new consensus object from it
    else:
        # create a new consensus path and resolve it
        index_child_starting_kmer = 0 # 0 since it is the first kmer of the child consensus
        child_consensus = Consensus(child_starting_kmer, father_consensus_id)
        child_consensus_id = child_consensus.consensus_id

    # optimisation # do not create the link between father and child if it is short and loop back to the consensus
    if child_consensus.ending == father_consensus_id and child_consensus.last_kmer_index <= Consensus.kmer_size:
        return

    if father_consensus_id != "start":
        father_consensus = Consensus.consensus_dict[father_consensus_id]
        father_consensus.linked_consensus[split_kmer_index] = father_consensus.linked_consensus.get(split_kmer_index, []) + [[child_consensus_id, index_child_starting_kmer]]
        father_consensus.split_list.append(split_kmer_index)
    child_consensus.split_list.append(index_child_starting_kmer)


    #print("new child path",father_consensus_id,"->",child_consensus_id)


def get_and_save_all_consensus(output_path: str, print_paths=False) -> None:
    """
    get all the scored completed paths from the consensus
    use the consensus built from starters kmers and get all the complete paths generated from
    save the result dict in a fasta format
    """

    # sort the split indexes of all consensus in a dict, each split points to the following split
    for consensus in Consensus.consensus_dict.values():

        # also add the last kmer of the consensus
        sorted_split_list = sorted(consensus.split_list) + [consensus.last_kmer_index]
        for i in range(len(sorted_split_list)-1):
            consensus.split_dict[sorted_split_list[i]] = sorted_split_list[i+1]


    scored_path_dict = {} # key: path string, value: associated score

    # loop over starters in order of decreasing occurrences
    for start_kmer in Consensus.starters_list[::-1]:
        # merge the computed dict with the main one
        starter_consensus_id = Consensus.kmers_dict.get(start_kmer, None)
        starter_consensus = Consensus.consensus_dict.get(starter_consensus_id, None)
        if starter_consensus is not None:
            starter_consensus.get_all_path_from_kmer(0, {})
            if Consensus.seq_size:
                paths_from_starter_dict = {k:v for k, v in starter_consensus.sub_parts[0].items() if len(k) == Consensus.seq_size}
            else:
                paths_from_starter_dict = starter_consensus.sub_parts[0]
            scored_path_dict.update(paths_from_starter_dict)

    # sort by descending score
    sorted_scored_path_dict = dict(sorted(scored_path_dict.items(), key=lambda item: item[1], reverse=True))

    if print_paths:
        for i, (sequence, score) in enumerate(sorted_scored_path_dict.items()):
            print("path", str(i+1), ": score of",score, "with len of", len(sequence))
    else:
        print("\n",len(scored_path_dict),"paths found")


    result_dict = {"consensus_"+str(i)+"_score="+str(score) : sequence for i, (sequence, score) in enumerate(sorted_scored_path_dict.items())}

    # save all the resulting consensus
    dfr.save_dict_to_fasta(result_dict, output_path)



def print_gv_graph() -> None:
    """
    display a graphviz graph of all consensus path
    only used for a visual representation of the consensus
    """

    gv_graph = graphviz.Digraph(comment='Consensus_graph')
    gv_graph.attr(ranksep='2.0', rankdir='LR') # ranksep to increase the distance between nodes, rankdir LR to force left to right

    start_sequence = Consensus.start_sequence
    stop_sequence = Consensus.stop_sequence

    #global placed_nodes_num
    placed_nodes_num = 0

    def gv_add_node(kmer):
        """
        add a node to the graph
        """
        #print(kmer, label)
        #if '\t'+kmer in gv_graph.source: # test if the node isn't already in the graph
            #print("duplicate node")
            #return

        nonlocal placed_nodes_num # get the function variable

        node_name = str(placed_nodes_num)

        placed_nodes_num += 1

        gv_graph.node(kmer, node_name)

    # add 2 nodes for start and stop primers
    gv_graph.node(start_sequence, "Start_Primer_Fw", peripheries='2')
    gv_graph.node(stop_sequence, "Stop_Primer_Rv", peripheries='2')


    # create edges between linked consensus

    created_nodes_dict = {} # key=consensus, value=dict of nodes
    nodes_sons = {} # key kmer, value = list of [son, distance, score]
    nodes_fathers = {} # key:kmer, value list of fathers kmers

    for consensus_id, consensus in Consensus.consensus_dict.items():

        # the dict can have been created by a parent consensus
        consensus_nodes_dict = created_nodes_dict.get(consensus, {}) # key: position in its consensus, value:kmer

        # init node for the starting kmer of this consensus
        start_kmer = consensus.position_to_kmer_dict[0]
        consensus_nodes_dict[0] = start_kmer

        # if the consensus starts with the start primer,
        if start_kmer.startswith(start_sequence) and start_kmer != start_sequence:
            nodes_sons[start_sequence] = nodes_sons.get(start_sequence, []) + [[start_kmer, 0, Consensus.kmer_occurrences_dict[start_kmer]]]
            nodes_fathers[start_kmer] = nodes_fathers.get(start_kmer, []) + [start_sequence]

        # add nodes that are linked to another consensus and the linking edge
        for index_last_kmer_before_child, linked_consensus_at_kmer in consensus.linked_consensus.items():

            for linked_consensus_id, index_first_kmer_from_child in linked_consensus_at_kmer:
                linked_consensus = Consensus.consensus_dict.get(linked_consensus_id, None)
                if linked_consensus is None: # has been removed from the dict
                    continue

                last_kmer_before_child = consensus.position_to_kmer_dict[index_last_kmer_before_child]
                consensus_nodes_dict[index_last_kmer_before_child] = last_kmer_before_child

                first_kmer_from_child = linked_consensus.position_to_kmer_dict[index_first_kmer_from_child]
                nodes_sons[last_kmer_before_child] = nodes_sons.get(last_kmer_before_child, []) + [[first_kmer_from_child, 1, Consensus.kmer_occurrences_dict[first_kmer_from_child]]]
                nodes_fathers[first_kmer_from_child] = nodes_fathers.get(first_kmer_from_child, []) + [last_kmer_before_child]

                # also create a node in the linked consensus
                linked_consensus_nodes_dict = created_nodes_dict.get(linked_consensus, {})
                linked_kmer_position = linked_consensus.kmer_to_position_dict[first_kmer_from_child]
                linked_consensus_nodes_dict[linked_kmer_position] = first_kmer_from_child

                created_nodes_dict[linked_consensus] = linked_consensus_nodes_dict


        # init node for the ending kmer of this consenus
        stop_kmer = consensus.position_to_kmer_dict[len(consensus.kmer_to_position_dict) - 1]
        consensus_nodes_dict[len(consensus.kmer_to_position_dict) - 1] = stop_kmer

        # if the consensus ends with the stop primer, add the edge
        if stop_kmer.endswith(stop_sequence) and stop_kmer != stop_sequence:
            nodes_sons[stop_kmer] = nodes_sons.get(stop_kmer, []) + [[stop_sequence, 0, Consensus.kmer_occurrences_dict[stop_kmer]]]
            nodes_fathers[stop_sequence] = nodes_fathers.get(stop_sequence, []) + [stop_kmer]


        # save the nodes related to this consensus
        created_nodes_dict[consensus] = consensus_nodes_dict


    # create edges between nodes of the same consensus

    for consensus in Consensus.consensus_dict.values():

        last_node_position = 0 # init

        for kmer_position in range(1, len(consensus.kmer_to_position_dict)):

            if kmer_position in created_nodes_dict.get(consensus, {}):
                node_kmer_from = created_nodes_dict[consensus][last_node_position]
                node_kmer_to = created_nodes_dict[consensus][kmer_position]
                distance = kmer_position-last_node_position # number of kmers between the 2 nodes
                _, score = consensus.get_intern_path_and_points_between_kmer(last_node_position, kmer_position)

                nodes_sons[node_kmer_from] = nodes_sons.get(node_kmer_from, []) + [[node_kmer_to, distance, score]]
                nodes_fathers[node_kmer_to] = nodes_fathers.get(node_kmer_to, []) + [node_kmer_from]

                last_node_position = kmer_position

    for node, sons_list in nodes_sons.items():
        if node == start_sequence or len(sons_list) != 1 or len(nodes_fathers.get(node, [])) > 1: # place the first node
            for son, distance, score in sons_list:
                edge_placed = False
                while not edge_placed:

                    if son == stop_sequence: # hide complete trivial paths
                        if node == start_sequence:
                            edge_placed = True
                            continue
                        gv_graph.edge(node, son, weight=str(score), label="d="+str(distance)+"\ns="+str(score))
                        edge_placed = True

                    elif len(nodes_sons.get(son, [])) != 1 or len(nodes_fathers[son]) > 1:
                        # place the node
                        gv_add_node(son)
                        gv_graph.edge(node, son, weight=str(score), label="d="+str(distance)+"\ns="+str(score))
                        edge_placed = True
                    else:
                        next_son, next_distance, next_score = nodes_sons[son][0] # transfer to the son's son
                        son = next_son
                        distance += next_distance
                        score = min(score, next_score)


    #print(gv_graph.source)
    gv_graph.render('Consensus_graph.gv', view=True) # save the graph and display it



class Consensus:
    """
    class representing a consensus path of kmers from the reads
    can also be a subpath between 2 kmers of other consensus, or between a kmer and the end
    """

    def __init__(self, first_kmer: str, origin_consensus_id: str) -> None:
        """
        init a consensus object
        assign a unique id number
        add the first kmer of this consensus
        origin_consensus_id is the consensus id this consensus is from, or "start" if it is a starter consensus
        """
        self.consensus_id = str(Consensus.consensus_number) # give this object an unique identifier
        Consensus.consensus_number += 1 # global number of consensus objects created
        Consensus.consensus_dict[self.consensus_id] = self # put this object in the dict of consensus

        self.kmer_to_position_dict = {} # key:kmer, value:position # position is relative to the consensus
        self.position_to_kmer_dict = {} # key:position, value:kmer # inversion of preceding dict

        self.last_kmer_index = -1 # -1 for empty dict # else, index of the last added kmer

        self.origin = origin_consensus_id # the consensus id this consensus is from, or "start" if it is a starter consensus

        self.ending = None
        # None when the consensus has not been finished,
        # else "{consensus_id}" if ended by reaching another consensus,
        # or "reach_end" if ended by reaching the stop primer,
        # or "dead_end" if not following kmers found to continue

        self.linked_consensus = {} # dict to save the other consensus linked from this one # key=index_kmer_before_split; value=list of [linked_consensus_id, linked_split_kmer] because multiple consensus can be linked from the same kmer

        self.sub_parts = {} # save the builded sub_paths from this consensus to avoid calculate them multiple times # key=first_kmer_of_builded_paths;value=path_dict

        self.split_list = [] # list of indexes where the consenus join/is by joined by another consensus
        self.split_dict = {} # dict of split_index:next_following_plit_index, the last one is followed by the last kmer of the consensus

        self.add_next_kmer(first_kmer) # init the path by adding its first kmer
        
        self.resolve_consensus() # build the consensus from the start kmer

    
    def add_next_kmer(self, new_kmer: str) -> None:
        """    
        add a kmer to continue the consensus
        if it is a never seen before kmer, associate it to the consensus, else it means that the consensus is linked to another consensus with this kmer
        """
        #print("add kmer", self.consensus_id,new_kmer)


        if new_kmer in self.kmer_to_position_dict: # Loop : the kmer is already present in the same consensus
            self.ending = self.consensus_id # stop the consensus to avoid infinite looping
            #self.linked_consensus[self.last_kmer_index] = self.linked_consensus.get(self.last_kmer_index, []) + [[self.consensus_id, self.kmer_to_position_dict[new_kmer]]] # index of split position
            #print("C" + self.consensus_id, "loop on kmer", new_kmer)
            
        elif new_kmer in Consensus.kmers_dict: # kmer is already contained in another consensus # create a link and stop the consensus
            linked_consensus_id = Consensus.kmers_dict[new_kmer] # get the id of the other consensus
            linked_consensus = Consensus.consensus_dict[linked_consensus_id]
            new_kmer_index_in_linked_consensus = linked_consensus.kmer_to_position_dict[new_kmer]

            self.linked_consensus[self.last_kmer_index] = self.linked_consensus.get(self.last_kmer_index, []) + [[linked_consensus_id, new_kmer_index_in_linked_consensus]] # index of split position
            linked_consensus.split_list.append(new_kmer_index_in_linked_consensus)
            self.split_list.append(self.last_kmer_index)
            self.ending = linked_consensus_id

            #print(self.consensus_id, " linked to other consensus", self.consensus_id,"->",linked_consensus_id)
        else: # register the new kmer
            self.last_kmer_index += 1 # update position of last new_kmer
            self.kmer_to_position_dict[new_kmer] = self.last_kmer_index # link the new_kmer with its position in the 2 dicts
            self.position_to_kmer_dict[self.last_kmer_index] = new_kmer
            
            Consensus.kmers_dict[new_kmer] = self.consensus_id # this consensus become the "owner" of this new_kmer # any consensus that reach this new_kmer will be linked to this path

            if new_kmer.endswith(Consensus.stop_sequence): # when reached the stop new_kmer of the consensus # happy ending
                self.ending = "reach_end"


    def resolve_consensus(self) -> None:
        """
        from the starting kmer of this consensus, try to reach the final stop kmer
        build an overlapping list of kmers by choosing the next kmer with the most occurrences
        stop when reached the end, or starting to loop, or not finding other overlapping kmer to continue, or reached another consensus
        can start and resolve subconsensus when the second kmer with the most occurrences is non-negligible
        """

        while self.ending is None: # while the consensus has not stopped by reaching the end,
            
            current_kmer = self.position_to_kmer_dict[self.last_kmer_index] # last kmer of the ongoing path


            #print(len(total_path))
            next_kmers_dict = {} # contains the following overlapping kmers and their respective weights
            
            printer = current_kmer+" "
            # get the occurrence score for each 4 possible next overlapping kmers
            for next_base in ["A","C","G","T"]:
                next_kmer = current_kmer[1:]+next_base         
                next_weight = Consensus.kmer_occurrences_dict.get(next_kmer, 0) # get the number of occurrences in the reads
                
                next_kmers_dict[next_kmer] = next_weight
                
                if next_weight > 0:
                    printer += next_base+":"+str(next_weight)+" "
                   
            #print(printer)
            
            sorted_next_kmers = sorted(next_kmers_dict, key=next_kmers_dict.get, reverse=True) # sort the next kmers by weight
            best_next_kmer = sorted_next_kmers[0]
            
            # no best base found with a non-null weight, bad ending
            if next_kmers_dict[best_next_kmer] == 0:
                #print("kmer_consensus warning : not enough reads for the full reconstruction")
                #print("C"+self.consensus_id,"path ended because no weight at",current_kmer)
                self.ending = "dead_end" # end the path search
            
            else:
                # the chosen next kmer is added to the path
                self.add_next_kmer(best_next_kmer) # can stop here if the added kmer contains the ending sequence
                # create another alternative consensus if other next kmers has a weight

                for other_next_kmer in sorted_next_kmers[1:]:
                    other_next_kmer_score = next_kmers_dict[other_next_kmer]

                    if other_next_kmer_score == 0:
                        break

                    else:    #print(next_kmers_dict[other_next_kmer], other_next_kmer)
                        # get the split position, != self.last_kmer_index because it depends on if the add_next_kmer stopped the consensus or not
                        split_kmer_index = self.kmer_to_position_dict[current_kmer]

                        # insert the data to create another consensus to explore
                        new_consensus_data = [other_next_kmer_score, self.consensus_id, split_kmer_index, other_next_kmer]
                        Consensus.list_of_consensus_to_explore.insert(0, new_consensus_data)

                        not_inserted = True

                        # find the index to insert the data to keep the list sorted by score
                        for i, waiting_consensus_data in enumerate(Consensus.list_of_consensus_to_explore):
                            if waiting_consensus_data[0] >= other_next_kmer_score:
                                Consensus.list_of_consensus_to_explore.insert(i, new_consensus_data)
                                not_inserted = False
                                break
                        if not_inserted:
                            # add the element at the very end
                            Consensus.list_of_consensus_to_explore.insert(-1, new_consensus_data)


    def get_intern_path_and_points_between_kmer(self, start_kmer_index: int, stop_kmer_index: int) -> (str, int):
        """
        get a subpath from the beginning of one kmer to the first base of the other kmer
        number of points is the minimum of occurrences for all the kmers in the intern path
        return first base and number of occurrences when begin_kmer == end_kmer
        """
        #print("intern path for consensus", self.consensus_id, "between", begin_kmer, "and", end_kmer)

        begin_kmer = self.position_to_kmer_dict[start_kmer_index]
        path = begin_kmer[0]

        min_kmer_occurrences = Consensus.kmer_occurrences_dict.get(begin_kmer, 0)

        for position in range(start_kmer_index+1, stop_kmer_index+1):
            kmer = self.position_to_kmer_dict[position]
            path += kmer[0]
            min_kmer_occurrences = min(min_kmer_occurrences, Consensus.kmer_occurrences_dict.get(kmer, 0))

        return path, min_kmer_occurrences


    def get_all_path_from_kmer(self, first_kmer_index: int, call_history: dict, rec=0):
        """
        get the list of path from this consensus and starting from one of its kmers
        recursive call
        save the result in a dict associated to the consensus object
        """
        #print("C"+self.consensus_id, first_kmer_index, "<")
        #print(self.split_list)
        #print(self.split_dict)

        # avoid to exceed recursion limit
        if rec + 10 > sys.getrecursionlimit():
            self.sub_parts[first_kmer_index] = self.sub_parts.get(first_kmer_index, {})
            print("recursion limit exceeded")
            return

        if call_history.get(self.consensus_id, -1) >= first_kmer_index:
            self.sub_parts[first_kmer_index] = self.sub_parts.get(first_kmer_index, {})
            #print("already called in this recursion")
            return
        else:
            call_history[self.consensus_id] = first_kmer_index

        # optimization, if this function has already been called with the same parameter, the result is the same as the last time
        if first_kmer_index in self.sub_parts:
            #print("path already computed C"+self.consensus_id+" from "+str(first_kmer_index))
            return

        else:
            # init an empty path from this kmer
            # this is replaced by the computed dict of path at the end of this function
            # but since it is a recursive function, if the recursion happens to ask again for this list of path, it means that it got in a loop
            # then it will return an empty list to avoid looping recursively indefinitely
            self.sub_parts[first_kmer_index] = {}

        # dict of path_sequence:points to save what is calculated here
        path_dict = {}

        first_kmer = self.position_to_kmer_dict[first_kmer_index]
        first_kmer_occ = Consensus.kmer_occurrences_dict[first_kmer]
        # when eaching the end of the consensus path
        if first_kmer_index == self.last_kmer_index:
            if self.ending == "reach_end": # get the path to the very end
                path_to_end, points_to_end = first_kmer, first_kmer_occ
                path_dict[path_to_end] = points_to_end
            # else just don't add a path because it is either a dead-end, or either a link to another consensus that is computed after

        else:
            # compute the path until the next split point
            # and add it to all the paths returned by the recursive call of this function on the next split point

            #print("C"+self.consensus_id, self.split_dict)
            #print(self.last_kmer_index)
            next_split_index = self.split_dict[first_kmer_index]
            path_until_next_split, points_until_next_split = self.get_intern_path_and_points_between_kmer(first_kmer_index, next_split_index-1)
            # compute all the paths from the split (recursion)
            self.get_all_path_from_kmer(next_split_index, call_history.copy(), rec+1)
            # add the paths
            for path, points in self.sub_parts[next_split_index].items():
                total_path = path_until_next_split + path
                total_points = min(points_until_next_split, points)
                path_dict[total_path] = total_points

        # loop over the linked consensus at the kmer
        for child_consensus_id, first_kmer_index_in_child in self.linked_consensus.get(first_kmer_index, []):
            child_consensus = Consensus.consensus_dict.get(child_consensus_id)
            # compute their path starting from the linked kmer (recursion)
            child_consensus.get_all_path_from_kmer(first_kmer_index_in_child, call_history.copy(), rec+1)

            # add the paths
            for path_from_child, points_from_child in child_consensus.sub_parts[first_kmer_index_in_child].items():
                total_path = first_kmer[0] + path_from_child
                total_points = min(first_kmer_occ, points_from_child)
                # ignore the result path if it is higher than the espected result size,
                # this test avoid gigantic results in paths with multiple shared kmers
                if Consensus.seq_size and len(total_path) > Consensus.seq_size:
                    continue
                path_dict[total_path] = total_points
                #print(len(total_child_path))

        # safety mesure
        if len(path_dict) > 200000:
            print("too many paths to explore, exit")
            exit(0)

        # save the result dict of path:points for this kmer to save time when if the function is called again with the same index
        self.sub_parts[first_kmer_index] = path_dict

        #print("> C"+self.consensus_id, first_kmer_index, str(len(path_dict)))



def pre_filter(sequence: str) -> bool:
    """
    filter the strange sequences, that are likely sequencing errors
    True if bad sequence, False otherwise
    """

    bases = ["A", "C", "G", "T"]

    # size h homopolymeres
    h = 10
    if any(base*h in sequence for base in bases):
        #print("removed homopolymere : ",sequence)
        return True
    
    # size h2 pairs of bases repetitions that clearly are bugs
    h2 = 10
    if any((base1+base2)*h2 in sequence for base1 in bases for base2 in bases):
        #print("removed 2-repetition : ",sequence)
        return True

    if any((base1+base2+base3)*h2 in sequence for base1 in bases for base2 in bases for base3 in bases):
        #print("removed 3-repetition : ",sequence)
        return True

    if any((base1+base2+base3+base4)*h2 in sequence for base1 in bases for base2 in bases for base3 in bases for base4 in bases):
        #print("removed 3-repetition : ",sequence)
        return True

    #if any((base1+base2+base3+base4+base5)*h2 in sequence for base1 in bases for base2 in bases for base3 in bases for base4 in bases for base5 in bases):
        #print("removed 3-repetition : ",sequence)
    #    return True

    return False


def kmer_consensus(input_path: str, output_path: str, start_primer: str, stop_primer: str, kmer_size: int, min_occ: int, seq_size: int) -> None:
    """
    get the consensus sequence from an ordered fragments assembly
    find a list of overlapping kmers from the dict of kmer occurrences
    build the original sequence from the start_primer and stop when matching the stop_primer    
    """
    
    start = time.time()
    """
    reads_dict = dfr.read_fastq(input_path)

    filtered_dict = {k:v for k, v in reads_dict.items() if not pre_filter(v)}

    filtered_seq_path = input_path.replace(".fastq", "_filtered.fastq")
    dfr.save_dict_to_fastq(filtered_dict, filtered_seq_path)

    filter_time = time.time() - start
    print("filtered "+str(round(filter_time, 3)))
    exit(0)"""
    start = time.time()

    # count the occurrences of kmers in the reads
    kmer_occurrences_dict = dsk.count_kmers(input_path, kmer_size, min_occ)
    
    # adjust the size of start and stop primers if it's longer than the kmer size
    if kmer_size < len(start_primer):
        start_primer = start_primer[:kmer_size]
    if kmer_size < len(stop_primer):
        stop_primer = stop_primer[:kmer_size]
    
    count_time = time.time() - start
    print("count kmer "+str(round(count_time, 3)))
    start = time.time()

    # init the global consensus class and start the consensus from the start_primer
    build_all_consensus(kmer_occurrences_dict, kmer_size, start_primer, stop_primer, seq_size)

    consensus_time = time.time() - start
    print("paths creation " +str(round(consensus_time, 3)))
    #start = time.time()

    # display a graph of the consensus paths found
    #print_gv_graph()

    #graph_time = time.time() - start
    #print("print graph "+str(round(graph_time, 3)))
    start = time.time()

    get_and_save_all_consensus(output_path, False)

    build_time = time.time() - start

    print("build all paths "+str(round(build_time, 3)))


if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='get the original complete sequence from sequencing reads of ordered assembly')
    parser.add_argument('-i', action='store', dest='input_path', required=True,
                        help='fastq/fasta file to read the sequences')
    parser.add_argument('-o', action='store', dest='output_path', required=True,
                        help='fasta file to save the result')
    parser.add_argument('--start', action='store', dest='start_primer', required=True,
                        help='start primer at the beginning of the sequence to reconstruct')
    parser.add_argument('--stop', action='store', dest='stop_primer', required=True,
                        help='stop primer at the beginning of the reverse complement of the sequence to reconstruct')
    parser.add_argument('--kmer_size', action='store', dest='kmer_size', required=False,
                        default=31, type=int, help='length of the kmers searched, arbitrary constant, increase the risk of finding loop if too short')
    parser.add_argument('--min_occ', action='store', dest='min_occ', required=False,
                        default=10, type=int, help='minimum occurrences for kmers to be used in the consensus')
    parser.add_argument('--seq_size', action='store', dest='seq_size', required=False,
                        default=None, type=int, help='espected size of the result consensus sequences')
    
    arg = parser.parse_args()
    
    print("reads consensus...")

    kmer_consensus(arg.input_path, arg.output_path, arg.start_primer, arg.stop_primer, arg.kmer_size, arg.min_occ, arg.seq_size)
                
    print("\tcompleted !")

