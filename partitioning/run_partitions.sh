#!/bin/bash


dir_path=test_10k
read_number=10000
# metis params
cluster_number=30
cluster_gap=10 # % of size gap between clusters

data_path=$dir_path/data

mkdir $dir_path
mkdir $data_path

# generate reads, shuffle
python3 read_matrix.py $data_path $read_number


:<<'END_COMMENT'
# ______________ test metis ______________ :

start_metis=$(date +%s.%N)
# convert reads to adjacent graph
graph_path=$dir_path/reads.graph
./partitioning $data_path/shuffled_reads.fastq $graph_path --graph
# run metis
gpmetis $graph_path $cluster_number -ufactor $cluster_gap > $dir_path/metis.log

end_metis=$(date +%s.%N)
runtime=$(echo "$end_metis - $start_metis" | bc)
echo $runtime s for full metis algorithm

# convert the output file
metis_output_file=$graph_path".part."$cluster_number
formated_metis_output=$dir_path"/metis_output.txt"
python3 -c "from partitioning import metis_output_to_soluce; metis_output_to_soluce(\"$metis_output_file\", \"$formated_metis_output\")"
rm $metis_output_file

# eval metis solution
metis_result=$dir_path/metis_result.txt
python3 -c "from partitioning import eval_soluce; eval_soluce(\"$data_path/soluce.txt\",\"$formated_metis_output\", \"$metis_result\")"

END_COMMENT

# ______________ test kp ______________ :

start_kp=$(date +%s.%N)
# convert reads to minimizer matrix at csvbm format
./partitioning $data_path/shuffled_reads.fastq $dir_path/matrix.csvbm --csvbm

. kp_run.sh -m $dir_path/matrix.csvbm -o $dir_path/kp_output


end_kp=$(date +%s.%N)
runtime=$(echo "$end_kp - $start_kp" | bc)

# eval kp solution
kp_result=$dir_path/kp_output/compacted_clusters.txt
python3 -c "from partitioning import eval_soluce; eval_soluce(\"$data_path/soluce.txt\",\"$kp_result\", \"$dir_path/kp_result.txt\")"
