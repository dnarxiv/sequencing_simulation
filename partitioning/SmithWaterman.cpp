#include "SmithWaterman.hpp"

SmithWatermanWorker::SmithWatermanWorker(const size_t primer_size_) : primer_size(primer_size_) {}

SmithWatermanWorker::AlignmentResult SmithWatermanWorker::align(const char *seq, const uint32_t seq_size,
																const char *primer) {
	// Reset data
	for (size_t j = 0; j <= primer_size; ++j) {
		line0[j] = 0;  // First line
	}
	line1[0] = 0;  // First column

	auto prev_line = line0;
	auto current_line = line1;
	score_t *tmp;

	// Fill matrix and remember max
	AlignmentResult res = {0, 0};
	for (size_t i = 1; i <= seq_size; ++i) {
		for (size_t j = 1; j <= primer_size; ++j) {
			current_line[j] = std::max(prev_line[j - 1] + sw_similarity(seq[i - 1], primer[j - 1]), 0);
			current_line[j] = std::max(current_line[j], prev_line[j] + SW_GAP_SCORE);
			current_line[j] = std::max(current_line[j], current_line[j - 1] + SW_GAP_SCORE);
			if (current_line[j] >= res.score) {
				res.score = current_line[j];
				res.pos = i;
			}
		}
		// Swap the two pointers
		tmp = prev_line;
		prev_line = current_line;
		current_line = tmp;
	}

	return res;
}

SmithWatermanWorker::AlignmentResult SmithWatermanWorker::align_reverse(const char *seq, const uint32_t seq_size,
																		const char *primer) {
	// Reset data
	for (size_t j = 0; j <= primer_size; ++j) {
		line0[j] = 0;  // First line
	}
	line1[primer_size] = 0;	 // Last column

	auto prev_line = line0;
	auto current_line = line1;
	score_t *tmp;

	// Fill matrix and remember max
	AlignmentResult res = {0, seq_size};
	for (int i = seq_size - 1; i >= 0; --i) {
		for (int j = primer_size - 1; j >= 0; --j) {
			current_line[j] = std::max(prev_line[j + 1] + sw_similarity(seq[i], primer[j]), 0);
			current_line[j] = std::max(current_line[j], prev_line[j] + SW_GAP_SCORE);
			current_line[j] = std::max(current_line[j], current_line[j + 1] + SW_GAP_SCORE);
			if (current_line[j] >= res.score) {
				res.score = current_line[j];
				res.pos = i;
			}
		}
		// Swap the two pointers
		tmp = prev_line;
		prev_line = current_line;
		current_line = tmp;
	}

	return res;
}
