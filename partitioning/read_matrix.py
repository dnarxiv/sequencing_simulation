#!/usr/bin/python3

import os
import sys
import inspect
import time
import random


currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.dirname(os.path.dirname(currentdir))+"/synthesis_modules")
import dna_file_reader as dfr
import synthesis_simulation as ss


class Minimizer_matrix:
    """
    class to handle matrix stuff
    """

    def __init__(self) -> None:

        self.lines = [] # list of lines
        self.ones_counter = 0 # number of 1 in the matrix
        self.line_number = 0
        self.column_number = 0

    @classmethod
    def init_from_reads(cls, reads_path: str, minimizers_dict: dict):
        """
        init the matrix object from a .fastq of reads and a dict of minimizers from these reads
        """
        # create the object
        new_matrix = Minimizer_matrix()

        # read the .fastq file and get basic info
        reads_dict = dfr.read_fastq(reads_path)
        reads_name_list = list(reads_dict.keys())

        new_matrix.line_number = len(minimizers_dict)
        new_matrix.column_number = len(reads_dict)

        # construction of the matrix

        for minimizer in minimizers_dict.keys():
            minimizer_line = []
            for read_name in reads_name_list:
                read_seq = reads_dict[read_name]
                read_seq_rv = dfr.reverse_complement(read_seq)
                # test if minimizer is in the read
                if minimizer in read_seq or minimizer in read_seq_rv:
                    minimizer_line.append("1")
                    new_matrix.ones_counter += 1
                else:
                    minimizer_line.append("0")
            new_matrix.lines.append(minimizer_line)

        # show matrix density
        cases_counter = new_matrix.line_number*new_matrix.column_number
        print(str(new_matrix.ones_counter)+ "/"+ str(cases_counter) +" = "+ str(round(100*new_matrix.ones_counter/cases_counter, 1))+"%")

        return new_matrix


    @classmethod
    def init_from_matrix_csv(cls, matrix_csv_path):
        """
        init the matrix object from a csv file #TODO optional soluce file
        """
        # create the object
        new_matrix = Minimizer_matrix()

        with open(matrix_csv_path, 'r') as input_csv:
            input_csv.readline() # skip columns names
            line = input_csv.readline()
            while line != "":
                matrix_row = line.replace("\n","").split(",")[1:]
                new_matrix.lines.append(matrix_row)
                new_matrix.ones_counter += sum(int(k) for k in matrix_row)
                line = input_csv.readline()
        new_matrix.line_number = len(new_matrix.lines)
        new_matrix.column_number = len(new_matrix.lines[0])

        return new_matrix


    def get_representative(self):
        repr_list = []

        def prod_scalaire(line1, line2):
            sum = 0
            for k in range(self.column_number):
                if line1[k] == "1" and line2[k] == "1":
                    sum += 1
            print(sum)
            return sum

        base_repr = self.lines[868]
        base_sum = sum([int(k) for k in base_repr])
        repr_list.append(["m893", base_repr])

        for i, repr in enumerate(self.lines):
            if sum(int(k) for k in repr) < 25 or sum(int(k) for k in repr) > 70:
                continue

            if prod_scalaire(repr, base_repr) == 0:
                print("more",prod_scalaire(repr, base_repr))
                repr_list.append(["m"+str(i),repr])
                break

        for name, repr in repr_list:
            with open("representatives/repr_"+name+"_"+str(prod_scalaire(repr, base_repr))+".csv", 'w') as output_file:

                first_line = ""
                ones_number = 0
                for i, case in enumerate(repr):
                    if case == "1":
                        first_line += ",r"+str(i)
                        ones_number += 1
                output_file.write(first_line+"\n")
                output_file.write(name+",1"*ones_number)



    def print_matrix_to_csv(self, matrix_csv_path: str) -> None:
        """
        save the matrix to .csv format
        """
        with open(matrix_csv_path, 'w') as output_file:
            output_file.write("min,"+",".join("r"+str(k) for k in range(self.column_number))+"\n")
            for i, line in enumerate(self.lines):
                output_file.write("m"+str(i)+",")
                output_file.write(",".join(line)+"\n")


    def print_matrix_to_coo(self, matrix_coo_path: str) -> None:
        """
        save the matrix to COO format
        first line : line_number column_numer
        coord x y of a 1 each line
        """
        with open(matrix_coo_path, 'w') as output_matrix:
            output_matrix.write(str(self.line_number)+" "+str(self.column_number)+"\n")
            for i, line in enumerate(self.lines):
                for j, value in enumerate(line):
                    if value == "1":
                        output_matrix.write(str(i)+" "+str(j)+"\n")


    def print_matrix_to_csvbm(self, matrix_csvbm_path: str) -> None:
        """
        save the matrix to .csvbm format
        row_br column_nbr ones_nbr\n
        distance_to_first_one\n
        ...
        distance_to_next_one_from_last_one\n
        ...
        """
        with open(matrix_csvbm_path, 'w', encoding='utf-8') as csvbm_file:
            csvbm_file.write(
                f'{self.line_number} {self.column_number} {self.ones_counter} 0\n',
            )
            distance = 0
            for line in self.lines:
                for column in line:
                    if column == "1":
                        csvbm_file.write(f'{distance}\n')
                        distance = 1
                    else:
                        distance += 1


def shuffle_reads(reads_path, shuffled_reads_path, soluce_path):

    soluce_dict = {} # cluster_name : list of reads from the cluster

    # read the .fastq file and get basic info
    reads_dict = dfr.read_fastq(reads_path)
    reads_name_list = list(reads_dict.keys())

    # shuffle the reads and save the original clusters in a dict
    random.shuffle(reads_name_list)

    # save the shuffled reads
    shuffled_reads = {read_name : reads_dict[read_name] for read_name in reads_name_list}
    dfr.save_dict_to_fastq(shuffled_reads, shuffled_reads_path)

    # save the original clusters
    for i, read_name in enumerate(reads_name_list):
        read_cluster = int(read_name.split("_")[1])
        soluce_dict[read_cluster] = soluce_dict.get(read_cluster, []) + ["r"+str(i)]

    soluce_dict = dict(sorted(soluce_dict.items()))

    with open(soluce_path, 'w') as output_file:
        for cluster_name in soluce_dict.keys():
            output_file.write(str(cluster_name)+":"+",".join(soluce_dict[cluster_name])+"\n")



def generate_references_sequences(seq_number: int, references_path: str) -> dict:
    """
    create random sequences to use as references for the tests
    """

    h_max = 3 # maximum homopolymere size
    seq_size = 60 # length of sequences

    generated_seq_dict = {} # key:sequence_name, value:sequence

    for seq_num in range(seq_number):
        sequence = ""
        while len(sequence) < seq_size :
            alphabet = ["A", "G", "C", "T"]
            letter = random.choice(alphabet)
            if sequence[-h_max:] == letter*h_max:  # if the end of the sequence is a homopolymer of this letter
                alphabet.remove(letter)
                letter = random.choice(alphabet)  # then pick another one
            sequence += letter
        generated_seq_dict["ref_"+str(seq_num)] = sequence

    dfr.save_dict_to_fasta(generated_seq_dict, references_path)


def generate_random_reads(references_file_path: str, coverage_list: list, reads_path: str) -> None:
    """
    generate a read file containing simulated reads from referrences sequences
    with a coverage from the list
    """

    #i_error, d_error, s_error = 0.005, 0.01, 0.005 # ~2% error rate
    i_error, d_error, s_error = 0.01, 0.025, 0.01 # ~4% error rate

    ref_dict = dfr.read_fasta(references_file_path)

    output_read_dict = {}

    for i, (seq_name, sequence) in enumerate(ref_dict.items()):

        for j in range(coverage_list[i]):
            simulated_read_seq = ss.add_errors(sequence, i_error, d_error, s_error)
            output_read_dict[seq_name+"_"+str(j)] = simulated_read_seq

    dfr.save_dict_to_fastq(output_read_dict, reads_path)


def get_minimizers(reads_path: str) -> dict:
    """
    get a dict of minimizers from the read file
    """
    #TODO minimizers that can also be found in primers should not be used
    #TODO optimise the search (lot of time to gain)
    #

    start = time.time()

    window_size = 10 # window to look for the minimizer
    minimizer_size = 6 # length of minimizer kmer

    minimizer_dict = {} # key:minimizer_kmer, value:number of occurrence in reads

    reads_dict = dfr.read_fastq(reads_path)

    for read_seq in reads_dict.values():
        sequence_size = len(read_seq)
        read_seq_rv = dfr.reverse_complement(read_seq)

        for i in range(sequence_size-window_size+1):
            sub_fw = read_seq[i:i+window_size]
            sub_rv = read_seq_rv[sequence_size-window_size-i:sequence_size-i]

            minimizer = "Z"*minimizer_size # default to erase with first real minimizer found

            for j in range(window_size-minimizer_size+1): # this is far from an optimal search
                sub_minimizer = sub_fw[j:j+minimizer_size]
                if sub_minimizer < minimizer:
                    minimizer = sub_minimizer

                sub_minimizer = sub_rv[j:j+minimizer_size]
                if sub_minimizer < minimizer:
                    minimizer = sub_minimizer

            minimizer_dict[minimizer] = minimizer_dict.get(minimizer, 0) +1

    print(str(len(minimizer_dict))+" minimizers found in reads")

    print(time.time()-start,"s for dict")
    return minimizer_dict


def get_coverage_list(total_sum: int, min_coverage=25) -> list:
    """
    return a list of number for a coverage of each generated read
    total_sum is the required sum of all coverage in the list
    """
    coverage_list = []
    current_sum = 0

    while current_sum <= total_sum:
        # random coverage between min and 2*min
        coverage_i = random.randint(min_coverage, 2*min_coverage)
        current_sum += coverage_i
        coverage_list.append(coverage_i)

    # adjust the last cluster to remove the excedent
    coverage_list[-1] = coverage_list[-1]-(current_sum-total_sum)
    # sorted by highest->lowest
    coverage_list = sorted(coverage_list, reverse=True)

    #print(len(coverage_list),"clusters with a sum of",sum(coverage_list))

    return coverage_list


def init_reads(dir_path: str, read_number: int):
    """
    generate random reads, shuffle them and keep the original order in a file
    """

    ref_path = dir_path +"/references.fasta"
    reads_path = dir_path +"/reads.fastq"
    shuffled_reads_path = dir_path +"/shuffled_reads.fastq"
    solutions_path = dir_path +"/soluce.txt"

    coverage_list = get_coverage_list(read_number) # list of coverage for each reference

    generate_references_sequences(len(coverage_list), ref_path)
    generate_random_reads(ref_path, coverage_list, reads_path)
    shuffle_reads(reads_path, shuffled_reads_path, solutions_path)


def display_results(input_matrix_csv, results_dir, result_output):

    matrix_lines = []
    with open(input_matrix_csv, 'r') as input_matrix:
        input_matrix.readline() # skip columns names
        line = input_matrix.readline()
        while line != "":
            content = line.replace("\n","").split(",")
            matrix_lines.append(content[1:])
            line = input_matrix.readline()

    # get list of ordered lines and columns from results files
    ordered_lines = []
    ordered_columns = []

    for filename in sorted(os.listdir(results_dir)):
        file_path = os.path.join(results_dir, filename)
        with open(file_path, 'r') as input_matrix:
            ordered_columns += input_matrix.readline().replace("\n","").split(",")[1:]
            line = input_matrix.readline()
            while line != "":
                line_nbr = line.split(",")[0]
                ordered_lines.append(line_nbr)
                line = input_matrix.readline()

    print(ordered_columns)
    with open(result_output, 'w') as output_file:
        output_file.write(","+",".join(ordered_columns)+"\n")
        for line_name in ordered_lines:
            line_number = int(line_name[1:])
            matrix_line = line_name
            for column_name in ordered_columns:
                column_number = int(column_name[1:])
                matrix_line += ","+matrix_lines[line_number][column_number]
            #print(matrix_line)
            output_file.write(matrix_line + "\n")

    print(sorted(ordered_lines))



if __name__ == "__main__":


    print("generate reads...")

    dir_path =  sys.argv[1]
    read_number = int(sys.argv[2])

    init_reads(dir_path, read_number)

    #eval_soluce(dir_path+"soluce.txt", dir_path+"metis_soluce_10p.txt", dir_path+"metis_result_10p_2.txt")
    #matrix = Minimizer_matrix.init_from_matrix_csv(dir_path+"matrix_10k.csv")

    #matrix.get_representative()
    #display_results(dir_path+"/matrix_100/matrix.csv", dir_path+"/matrix_100_results", dir_path+"/matrix_100/matrix_100_ordered_results.csv")
    print("\tcompleted !")

