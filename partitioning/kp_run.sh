#!/usr/bin/env bash

# ---------------------------------------------------------------------------- #
# USAGE
#
# TODO
# ---------------------------------------------------------------------------- #
# get the absolute path of the kp_iter_bic directory
KP_DIR=$(realpath -m $(dirname "$BASH_SOURCE"))/matrix_tests/victor/kp_iter_bic


# get command line options
while getopts "m:o:" opt; do
  case $opt in
    m) csvbm_filepath="$OPTARG" ;;
    o) output_directory="$OPTARG" ;;
    \?) echo "Option invalide: -$OPTARG" >&2; exit 1 ;;
  esac
done

#csvbm_filepath=$KP_DIR"/tests/data/matrix_10k.csvbm"
max_number_of_rows=350
max_number_of_columns=350
min_density=0.8
percentage_of_zeros_to_delete=0.5
dimension_to_cut_strategy="largest_dimension_first"
#output_directory=$KP_DIR"/tests/result_1k_${max_number_of_rows}x${max_number_of_columns}_d${min_density//./}_p${percentage_of_zeros_to_delete//./}"

rm -rf $output_directory

$KP_DIR/target/release/kpiterbic --csvbm-filepath "$csvbm_filepath" \
    --max-number-of-rows "$max_number_of_rows" \
    --max-number-of-columns "$max_number_of_columns" \
    --min-density "$min_density" \
    --percentage-of-zeros-to-delete "$percentage_of_zeros_to_delete" \
    --dimension-to-cut-strategy "$dimension_to_cut_strategy" \
    --output-directory "$output_directory"
    
python3 -c "from partitioning import kp_iter_bic_output_to_soluce; kp_iter_bic_output_to_soluce(\"$output_directory/clusters\",\"$output_directory/compacted_clusters.txt\")"

# python3 $KP_DIR/src/clusters_to_soluce.py "$output_directory/clusters" $KP_DIR"/tests/soluce_kp_10k.txt"
