#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <list>
#include <bitset>
#include <unordered_map>
#include <cmath>
#include <algorithm>
#include <chrono>


int window_size = 10; // window to look for the minimizer
int minimizer_size = 6; // length of minimizer kmer


std::string reverse_complement(const std::string& sequence) {
    // get the reverse complement of a sequence
    std::string rc_sequence = sequence;
    std::transform(rc_sequence.begin(), rc_sequence.end(), rc_sequence.begin(), [](char c) {
        switch (c) {
            case 'A': return 'T';
            case 'C': return 'G';
            case 'G': return 'C';
            case 'T': return 'A';
            default: return c;
        }
    });
    std::reverse(rc_sequence.begin(), rc_sequence.end());
    return rc_sequence;
}


std::unordered_map<std::string, int> sequence_index_map;

void read_sequence_index_map(const std::string& filename) {
    // read the couple canonical minimizers : indexes from a file
    std::ifstream file(filename);
    if (!file.is_open()) {
        std::cerr << "Error opening file\n";
        return;
    }

    std::string sequence;
    int index;
    while (file >> sequence >> index) {
        sequence_index_map[sequence] = index;
    }
    file.close();
}

/* unused because less efficient than just loading a file with all indexes, which also avoid non-canonical minimizers
std::unordered_map<char, int> base_to_num_dict = {{'A', 0}, {'C', 1}, {'G', 2}, {'T', 3}};

// Function to convert a minimizer to a unique associated number
int minimizer_to_number(const std::string& minimizer) {
    int number = 0;
    for (char base : minimizer) {
        number = (number << 2) | base_to_num_dict[base];
    }
    return number;
}*/


int count_sequences(const std::string& input_fastq) {
    // get the number of sequences from the fastq
    std::ifstream file(input_fastq);
    std::string line;
    int seq_number = 0;

    while (std::getline(file, line)) {
        if (line[0] == '@') {
            seq_number++;
        }
    }
    return seq_number;
}

// function to read FASTQ file and return a vector of sequences
std::vector<std::string> read_fastq(const std::string& filename) {
    std::ifstream file(filename);
    std::vector<std::string> sequences;
    std::string line;

    if (!file.is_open()) {
        std::cerr << "Error opening file: " << filename << std::endl;
        return sequences;
    }

    // Skip the header line
    while (std::getline(file, line)) {

        // Read the sequence line
        std::getline(file, line);
        sequences.push_back(line);

        // Skip the quality score lines
        std::getline(file, line);
        std::getline(file, line);
    }

    file.close();

    return sequences;
}

// function to get a vector of minimizers from the read file
std::vector<std::vector<int>> get_minimizers(const std::string& filename) {

    int minimizers_number = sequence_index_map.size(); // get the number of minimizers of the dict

    std::vector<std::vector<int>> minimizer_list(minimizers_number); // each minimizer possible is associated to an index in the list,
                                                                     // list at index i = list of reads that contains minimizer i

    std::string default_minimizer = std::string(minimizer_size, 'Z'); // initialize a default minimizer a string of 'Z'

    std::vector<std::string> sequences = read_fastq(filename);

    for (int read_id = 0; read_id < sequences.size(); read_id++) {
        const std::string& read_seq = sequences[read_id];
        int sequence_size = read_seq.size();

        std::string read_seq_rv = reverse_complement(read_seq); // reverse complement of the read sequence
        std::string last_minimizer_added = default_minimizer; // initialize last minimizer added to a string of 'Z'

        for (int i = 0; i <= sequence_size - window_size; i++) {

            std::string minimizer = default_minimizer; // initialize minimizer to a string of 'Z'

            for (int j = 0; j <= window_size - minimizer_size; j++) {
                std::string sub_minimizer = read_seq.substr(i+j, minimizer_size); // forward substring of length minimizer_size
                if (sub_minimizer < minimizer) { // if sub_minimizer is smaller than current minimizer, update minimizer
                    minimizer = sub_minimizer;
                }

                sub_minimizer = read_seq_rv.substr(sequence_size-i-j-minimizer_size, minimizer_size); // reverse substring of length minimizer_size
                if (sub_minimizer < minimizer) { // if sub_minimizer is smaller than current minimizer, update minimizer
                    minimizer = sub_minimizer;
                }
            }

            if (minimizer != last_minimizer_added) { // avoid duplicates
                minimizer_list[sequence_index_map[minimizer]].push_back(read_id); // add read_id to the list of reads that contains minimizer
                last_minimizer_added = minimizer; // update last minimizer added
            }
        }
    }

    // Remove empty lists from the minimizer list
    minimizer_list.erase(std::remove_if(minimizer_list.begin(), minimizer_list.end(),
                                        [](const std::vector<int>& v) { return v.empty(); }),
                         minimizer_list.end());

    return minimizer_list;
}


void minlist_to_graph(const std::string& input_fastq, const std::string& graph_file_path) {

    // start the timer
    auto start = std::chrono::high_resolution_clock::now();

    // get the number of sequences from the fastq
    int seq_number = count_sequences(input_fastq);

    // compute the minimizers list
    std::vector<std::vector<int>> minimizer_list = get_minimizers(input_fastq);


    // create a vector of lists to store the graph
    std::vector<std::vector<int>> graph_lines(seq_number);


    // Iterate over the minimizer list
    for (const auto& neighbours_list : minimizer_list) {
        // Iterate over the neighbours list
        for (int i = 0; i < neighbours_list.size(); i++) {
            int read_id = neighbours_list[i];
            // Add the neighbours to the graph
            for (int j = 0; j < neighbours_list.size(); j++) {
                if (j != i) {
                    graph_lines[read_id].push_back(neighbours_list[j] + 1);
                }
            }
        }
    }

    // End the timer and print the elapsed time
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = end - start;
    std::cout << elapsed.count() << " create graph" << std::endl;

    // Calculate the number of edges in the graph
    int edge_number = 0;
    for (const auto& neighbours_list_by_id : graph_lines) {
        edge_number += neighbours_list_by_id.size();
    }
    edge_number /= 2;

    // Open the output file
    std::ofstream output_graph_file(graph_file_path);

    // Write the number of vertices and edges to the output file
    output_graph_file << seq_number << " " << edge_number << std::endl;
    
    // Write the graph to the output file
    for (const auto& neighbours_list_by_id : graph_lines) {
        for (const auto& neighbour : neighbours_list_by_id) {
            output_graph_file << neighbour << " ";
        }
        output_graph_file << std::endl;
    }

    // End the timer and print the elapsed time
    end = std::chrono::high_resolution_clock::now();
    elapsed = end - start;
    std::cout << elapsed.count() << "s for min_list of graph" << std::endl;
}


void minlist_to_csv(const std::string& input_fastq, const std::string& output_file) {

    // start the timer
    auto start = std::chrono::high_resolution_clock::now();

    // get the number of sequences from the fastq
    int seq_number = count_sequences(input_fastq);

    // compute the minimizers list
    std::vector<std::vector<int>> minimizer_list = get_minimizers(input_fastq);

    // Get the number of minimizers
    int min_list_size = minimizer_list.size();

    // Create a binary matrix to store the minimizer-read relationships
    std::vector<std::vector<int>> matrix(min_list_size, std::vector<int>(seq_number, 0));

    // Iterate over the minimizer list
    for (int i = 0; i < min_list_size; i++) {
        // Iterate over the list of reads that contain the current minimizer
        for (int read_id : minimizer_list[i]) {
            // Set the corresponding cell in the matrix to 1
            matrix[i][read_id] = 1;
        }
    }

    // Open the output file
    std::ofstream outfile(output_file);

    // Write the column headers to the output file
    outfile << "min,";
    for (int j = 0; j < seq_number; j++) {
        outfile << "r" << j;
        if (j < seq_number - 1) {
            outfile << ",";
        }
    }
    outfile << "\n";

    // Write the matrix to the output file in CSV format
    for (int i = 0; i < min_list_size; i++) {
        outfile << "m" << i << ",";
        for (int j = 0; j < seq_number; j++) {
            outfile << matrix[i][j];
            if (j < seq_number - 1) {
                outfile << ",";
            }
        }
        outfile << "\n";
    }

    // Close the output file
    outfile.close();

    // End the timer and print the elapsed time
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = end - start;
    std::cout << elapsed.count() << "s for minlist_to_csv" << std::endl;
}


void minlist_to_csvbm(const std::string& input_fastq, const std::string& output_file) {

    // start the timer
    auto start = std::chrono::high_resolution_clock::now();

    // get the number of sequences from the fastq
    int seq_number = count_sequences(input_fastq);

    // compute the minimizers list
    std::vector<std::vector<int>> minimizer_list = get_minimizers(input_fastq);

    // get the number of minimizers
    int min_list_size = minimizer_list.size();

    // Create a binary matrix to store the minimizer-read relationships
    std::vector<std::vector<int>> matrix(min_list_size, std::vector<int>(seq_number, 0));

    for (int i = 0; i < min_list_size; i++) {
        for (int read_id : minimizer_list[i]) {
            matrix[i][read_id] = 1;
        }
    }

    // open the output file
    std::ofstream outfile(output_file);

    // write the header line
    int line_number = matrix.size();
    int column_number = matrix[0].size();
    int ones_counter = 0;
    for (const auto& line : matrix) {
        for (int column : line) {
            if (column == 1) {
                ones_counter++;
            }
        }
    }
    outfile << line_number << " " << column_number << " " << ones_counter << " 0\n";

    // Write the data
    int distance = 0;
    for (const auto& line : matrix) {
        for (int column : line) {
            if (column == 1) {
                outfile << distance << "\n";
                distance = 1;
            } else {
                distance++;
            }
        }
    }

    // close the output file
    outfile.close();

    // end the timer and print the elapsed time
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = end - start;
    std::cout << elapsed.count() << "s for minlist_to_csvbm" << std::endl;
}


void csvbm_streaming(const std::string& reads_path, const std::string& csvbm_path) {
    /* read the fastq file, for each read, get the corresponding binary line of minimizers and write it in the csvbm format
     input : path to a fastq read file
     output : path to a csvbm file to be created
    this format represents a matrix where reads are the rows and minimizers the columns, allowing the streaming
    */
    // start a timer
    auto start = std::chrono::high_resolution_clock::now();

    std::string temp_csvbm_path = csvbm_path+"_temp"; // file to write the csvbm lines without the headder
    // the header content can only be computed after browsing all the reads

    std::ifstream input_read_file(reads_path);
    std::ofstream temp_output_file(temp_csvbm_path);

    int distance = 0; // distance with last "1" in the csvbm file
    int ones_counter = 0; // count the number of "1"
    int reads_counter = 0; // count the number of read sequences in the input file

    std::string line;
    int minimizers_number = sequence_index_map.size(); // get the number of minimizers of the dict
    std::string default_minimizer = std::string(minimizer_size, 'Z'); // initialize a default minimizer a string of 'Z'

    // browse the reads file and skip the header line
    while (std::getline(input_read_file, line)) {

        // read the sequence line
        std::getline(input_read_file, line);
        reads_counter++;

        std::vector<int> minimizer_list(minimizers_number, 0); // init a zeros list, each minimizer possible is associated to an index in the list

        int sequence_size = line.size();
        std::string read_seq_rv = reverse_complement(line); // reverse complement of the read sequence
        std::string last_minimizer_added = default_minimizer; // initialize last minimizer added to 'ZZZZZ...'

        for (int i = 0; i <= sequence_size - window_size; i++) {

            std::string minimizer = default_minimizer; // initialize minimizer to 'ZZZZZ...'
            for (int j = 0; j <= window_size - minimizer_size; j++) {
                std::string sub_minimizer = line.substr(i+j, minimizer_size); // forward substring of length minimizer_size
                if (sub_minimizer < minimizer) { // if sub_minimizer is smaller than current minimizer, update minimizer
                    minimizer = sub_minimizer;
                }

                sub_minimizer = read_seq_rv.substr(sequence_size-i-j-minimizer_size, minimizer_size); // reverse substring of length minimizer_size
                if (sub_minimizer < minimizer) { // if sub_minimizer is smaller than current minimizer, update minimizer
                    minimizer = sub_minimizer;
                }
            }

            if (minimizer != last_minimizer_added) { // avoid duplicates
                minimizer_list[sequence_index_map[minimizer]] = 1; // set 1 for the index corresponding to the minimizer
                last_minimizer_added = minimizer; // update last minimizer added
            }
        }

        std::string output_line;
        // Write the data in the output file
        for (const auto& minimizers_presence : minimizer_list) {
            if (minimizers_presence) { // if '1' in the corresponding index
                output_line += std::to_string(distance) + "\n";
                ones_counter++;
                distance = 1;
            } else {
                distance++;
            }
        }
        temp_output_file << output_line;


        // Skip the quality score lines
        std::getline(input_read_file, line);
        std::getline(input_read_file, line);
    }
    temp_output_file << std::to_string(distance-1) + "\n"; // write the distance to the last cell

    input_read_file.close();
    temp_output_file.close();

    // write the header line at the beginning of the csvbm

    std::ifstream temp_input_file(temp_csvbm_path); // reopen the file to read it from the beginning
    std::ofstream final_output_file(csvbm_path);

    // write the header
    // line number + columns number + "1" counter + is_reversed(=0)
    final_output_file << minimizers_number << " " << reads_counter << " " << ones_counter << " 0\n";

    while (std::getline(temp_input_file, line)) {
        final_output_file << line << "\n"; // copy all the lines to the final output file
    }

    temp_input_file.close();
    final_output_file.close();

    std::remove(temp_csvbm_path.c_str()); // remove temp file

    // end the timer and print the elapsed time
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = end - start;
    std::cout << elapsed.count() << "s for csvbm streaming" << std::endl;
}


int main(int argc, char* argv[]) {

    // check if the input and output file paths are provided as arguments
    if (argc != 4) {
        std::cerr << "Usage: " << argv[0] << " <input_fastq> <output_file> [--graph|--csv|--csvbm]" << std::endl;
        return 1;
    } // ./partitioning reads.fastq reads.graph --graph

    // get the input and output file paths from the arguments
    std::string input_fastq = argv[1];
    std::string output_file = argv[2];
    std::string option = argv[3];

    // read the list of all possible minimizers and the associated indexes
    read_sequence_index_map("minimizers_6.txt");

    // call the appropriate function based on the option
    if (option == "--graph") {
        minlist_to_graph(input_fastq, output_file);
    } else if (option == "--csv") {
        minlist_to_csv(input_fastq, output_file);
    } else if (option == "--csvbm") {
        //minlist_to_csvbm(input_fastq, output_file);
        csvbm_streaming(input_fastq, output_file);
    } else {
        std::cerr << "Invalid option: " << option << std::endl;
        return 1;
    }

    return 0;
}
//g++ -o partitioning partitioning.cpp
