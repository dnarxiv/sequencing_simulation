#!/usr/bin/python3

import os
import sys
import inspect
import subprocess
import time
import argparse

import kmer_counting_dsk as dsk

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.dirname(currentdir)+"/synthesis_modules")
import dna_file_reader as dfr

# used to get the consensus sequence from a .fastq reads file of an ordered assembly

KMER_SIZE = 35 # arbitrary constant, increase the risk of finding loop if too short


def pre_filter(sequence: str) -> bool:
    """
    filter the strange sequences, that are likely sequencing errors
    True if bad sequence, False otherwise
    """
    # ridiculously long sequences
    #if len(sequence) > 5000: #TODO careful, with a lot of fragments we may get very long sequences
        #print("removed too long : ",sequence, len(sequence))
        #return True
    
    bases = ["A", "C", "G", "T"]

    # size h homopolymeres
    h = 20
    if any(base*h in sequence for base in bases):
        #print("removed homopolymere : ",sequence)
        return True
    
    # size h2 pairs of bases repetitions that clearly are bugs
    h2 = 12
    if any((base1+base2)*h2 in sequence for base1 in bases for base2 in bases):
        #print("removed 2-repetition : ",sequence)
        return True
      
    return False


def build_result_sequence(total_path, current_kmer, stop_kmer, kmer_occurrences_dict, overlap_kmers_dict, path_list) -> list:
    
    while current_kmer != stop_kmer:
        
        #print(len(total_path))
        best_next_kmer, second_next_kmer = "", ""
        best_next_weight, second_next_weight = 0, 0
        
        printer = current_kmer+" "
        # get the occurrence score for each 4 possible next overlapping kmers
        for next_base in ["A","C","G","T"]:
            next_kmer = current_kmer[1:]+next_base
            next_weight = kmer_occurrences_dict.get(next_kmer, 0)
            
            if next_weight > 0:
                printer += next_base+":"+str(next_weight)+" "
            # keep the kmer with best occurrences score and use it to continue the path
            if next_weight > best_next_weight:
                best_next_kmer, second_next_kmer = next_kmer, best_next_kmer
                best_next_weight, second_next_weight = next_weight, best_next_weight
            elif next_weight > second_next_weight:
                second_next_kmer = next_kmer
                second_next_weight = next_weight
                
        #print(printer)

        # no best base found, bad ending
        if best_next_weight == 0:
            #print("kmer_consensus warning : not enough reads for the full reconstruction")
            #print("path ended at "+str(len(total_path)))
            return path_list
        
        # try the second path if it has a non negligible weight compared to best path and is not a loop
        #if second_next_weight > best_next_weight/2 and not overlap_kmers_dict.get(second_next_kmer, False):
        #    overlap_kmers_dict[second_next_kmer] = True
        #    path_list = build_result_sequence(total_path + second_next_kmer[-1], second_next_kmer, stop_kmer, kmer_occurrences_dict, overlap_kmers_dict.copy(), path_list)
        #    overlap_kmers_dict[second_next_kmer] = False
        
        
        # the chosen next base is placed at the right of the result sequence 
        total_path = total_path + best_next_kmer[-1]
        
        
        # the builded sequence is starting to loop -> very bad -> need to increase the kmer size
        if overlap_kmers_dict.get(best_next_kmer, False):
            print("! loop !",best_next_kmer, best_next_weight)
            #print(total_path)
            return path_list
        else:
            overlap_kmers_dict[best_next_kmer] = True
        
        current_kmer = best_next_kmer
    
    return [total_path] + path_list # happy ending because we reached the stop extremity
    

def build_with_known_extremities(start_sequence: str, stop_sequence: str, kmer_occurrences_dict: dict) -> str:
    """
    find a list of overlapping kmers from the dict of kmer occurrences
    build the original sequence from the start_sequence and stop when matching the stop_sequence    
    """
    
    if KMER_SIZE < len(start_sequence):
        start_sequence = start_sequence[:KMER_SIZE]
    if KMER_SIZE < len(stop_sequence):
        stop_sequence = stop_sequence[:KMER_SIZE]

    # find all kmers that begins with the start sequence
    potential_starters_dict = dict((k,v) for k,v in kmer_occurrences_dict.items() if k.startswith(start_sequence))
    # find all kmers that ends with the stop sequence
    potential_enders_dict = dict((k,v) for k,v in kmer_occurrences_dict.items() if k.endswith(dfr.reverse_complement(stop_sequence)))
    
    if len(potential_starters_dict) == 0:
        print("kmer_consensus error : starting sequence not found in the reads")
        return ""
    
    if len(potential_enders_dict) == 0:
        print("kmer_consensus error : ending sequence not found in the reads")
        return ""
    
    # the one with the most occurrences is considered as the starting kmer
    start_kmer = max(potential_starters_dict, key=potential_starters_dict.get) # kmer with the maximum occurrences that begins with the starting sequence 
    
    stop_kmer = max(potential_enders_dict, key=potential_enders_dict.get) # kmer with the maximum occurrences that ends with the stop sequence 
    
    # building
    
    overlap_kmers_dict = {start_kmer : True} # dict of overlapping kmers
    path_list = []
    total_path = start_kmer # builded sequence
    
    current_kmer = start_kmer
    
    path_list = build_result_sequence(start_kmer, start_kmer, stop_kmer, kmer_occurrences_dict, overlap_kmers_dict, path_list)

    print("paths",len(path_list))
    
    #print(path_list)
    return path_list


def build_from_start(start_sequence: str, kmer_occurrences_dict: dict, result_length=None) -> str:
    """
    find a list of overlapping kmers from the dict of kmer occurrences
    build the original sequence from the start_sequence and stop when no following kmers found
    """
    
    if KMER_SIZE < len(start_sequence):
        start_sequence = start_sequence[:KMER_SIZE]


    # find all kmers that begins with the start sequence
    potential_starters_dict = dict((k,v) for k,v in kmer_occurrences_dict.items() if k.startswith(start_sequence))

    if len(potential_starters_dict) == 0:
        print("kmer_consensus error : starting sequence not found in the reads")
        return ""

    overlap_kmers_dict = {} # dict of overlapping kmers
    
    # the one with the most occurrences is considered as the starting kmer
    start_kmer = max(potential_starters_dict, key=potential_starters_dict.get) # kmer with the maximum occurrences that begins with the starting sequence 
        
    total_path = start_kmer

    
    while True:
        
        best_added_kmer = ""
        best_added_weight = 0
            
        # get the occurrence score for each 4 possible next overlapping kmers
        for base in ["A","C","G","T"]:
            
            next_kmer = total_path[-KMER_SIZE+1:] + base
            if next_kmer == total_path[-KMER_SIZE:]: continue # avoid looping when an homopolymere of kmer_size is encountered
            next_weight = kmer_occurrences_dict.get(next_kmer, 0)

                
            # keep the kmer with best occurrences score and use it to continue the path
            if next_weight > best_added_weight:
                best_added_kmer = next_kmer
                best_added_weight = next_weight

        
        # no best base found, end the building and return the sequence
        if best_added_kmer == "":
            break
        
        total_path = total_path + best_added_kmer[-1]
        #print("->",best_added_kmer, kmer_occurrences_dict.get(best_added_kmer,0))
        
        
        # the builded sequence is starting to loop -> very bad -> need to increase the kmer size
        if overlap_kmers_dict.get(best_added_kmer, False):
            print("! loop !",best_added_kmer, best_added_weight)
            #print(total_path)
            return total_path
        else:
            overlap_kmers_dict[best_added_kmer] = True
            
        # exit when result length is reached if there is a result length defined
        if result_length != None and len(total_path) >= result_length:
            break
        
    return total_path



def build_without_extremities(kmer_occurrences_dict: dict, result_length=None) -> str:
    """
    build the sequence without known extremities, starts from the most common kmer
    can precise length of the result to stop when it is achieved, else stops when no next kmers from the dictionary of occurrences
    warning : 1/2 chance to get the reverse complement of the result with this method since the kmers are in the 2 orders
    """
    
    overlap_kmers_dict = {} # dict of overlapping kmers
    
    most_common_kmer = max(kmer_occurrences_dict, key=kmer_occurrences_dict.get) # kmer that appeared the most in the reads
    
    total_path = most_common_kmer # builded sequence
    
    current_kmer = most_common_kmer # starting point 
    print(most_common_kmer)
    while True:
        
        best_added_kmer = ""
        best_added_weight = 0
        
        add_after = True
        
        # get the occurrence score for each 8 possible next overlapping kmers (4 before and 4 after the current builded sequence)
        for base in ["A","C","G","T"]:
            
            next_kmer = total_path[-KMER_SIZE+1:] + base         
            next_weight = kmer_occurrences_dict.get(next_kmer, 0)
            
            prev_kmer = base + total_path[:KMER_SIZE-1]        
            prev_weight = kmer_occurrences_dict.get(prev_kmer, 0)
                
            # keep the kmer with best occurrences score and use it to continue the path
            # chose to add after or before the current build depending on the scores of the before/after kmers
            if next_weight >= prev_weight and next_weight > best_added_weight:
                best_added_kmer = next_kmer
                best_added_weight = next_weight
                add_after = True # add after
                
            if prev_weight >= next_weight and prev_weight > best_added_weight:
                best_added_kmer = prev_kmer
                best_added_weight = prev_weight
                add_after = False # add before
        
        # no best base found, end the building and return the sequence
        if best_added_kmer == "":
            break
        
        # the chosen base is placed at the right or left of the result sequence
        if add_after:
            total_path = total_path + best_added_kmer[-1]
            print("->",best_added_kmer, kmer_occurrences_dict.get(best_added_kmer,0))
        else:
            total_path = best_added_kmer[0] + total_path
            print("<-",best_added_kmer, kmer_occurrences_dict.get(best_added_kmer,0))
        
        # the builded sequence is starting to loop -> very bad -> need to increase the kmer size
        if overlap_kmers_dict.get(best_added_kmer, False):
            print("! loop !",best_added_kmer, best_added_weight)
            #print(total_path)
            return total_path
        else:
            overlap_kmers_dict[best_added_kmer] = True
            
        # exit when result length is reached if there is a result length defined
        if result_length != None and len(total_path) >= result_length:
            break
        
    return total_path


def kmer_consensus(input_path: str, output_path: str, start_sequence: str, stop_sequence: str, min_occ: int) -> None:
    """
    get the consensus sequence from an ordered fragments assembly
    """
    
    #start = time.time()
        
    # count the occurrences of kmers in the reads
    kmer_occurrences_dict = dsk.count_kmers(input_path, KMER_SIZE, min_occ)
    
    #count_time = time.time() - start
    #start = time.time()

    # build the resulting sequence from the dictionary of following bases
    result_sequence_list = build_with_known_extremities(start_sequence, stop_sequence, kmer_occurrences_dict) 
    #result_sequence = build_from_start(start_sequence, kmer_occurrences_dict)
    print(result_sequence_list)
    
    #result_sequence = build_without_extremities(kmer_occurrences_dict)
    #build_time = time.time() - start
    
    results_dict = {"consensus_"+str(i) : sequence for i, sequence in enumerate(result_sequence_list)}
    dfr.save_dict_to_fasta(results_dict, output_path)
    #dfr.save_sequence_to_fasta("consensus", result_sequence, output_path)
    
    #print("\ncounting",count_time,"seconds")
    #print("building",build_time,"seconds")



if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='get the original complete sequence from sequencing reads of ordered assembly')
    parser.add_argument('-i', action='store', dest='input_path', required=True,
                        help='fastq file to read the sequences')
    parser.add_argument('-o', action='store', dest='output_path', required=True,
                        help='fasta file to save the result')
    parser.add_argument('--start', action='store', dest='start_primer', required=True,
                        help='start primer at the beginning of the sequence to reconstruct')
    parser.add_argument('--stop', action='store', dest='stop_primer', required=True,
                        help='stop primer at the beginning of the reverse complement of the sequence to reconstruct')
    parser.add_argument('--min_occ', action='store', dest='min_occ', required=False,
                        default=4, type=int, help='minimum occurrences for kmers to be used in the consensus')
    
    arg = parser.parse_args()
    
    print("reads consensus...")

    kmer_consensus(arg.input_path, arg.output_path, arg.start_primer, arg.stop_primer, arg.min_occ)
                
    print("\tcompleted !")

